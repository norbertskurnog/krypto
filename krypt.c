#include <stdlib.h>
#include <stdio.h>
#include <openssl/camellia.h>
#include <string.h>
#include <openssl/opensslconf.h>

#define ECB 1
#define CBC 2

CAMELLIA_KEY* key;

int main(int argc, char *argv[])
{
	key = malloc(sizeof(CAMELLIA_KEY));	

        int option;
        int mode;

        if(argc !=  6) {
                perror("");
                return 1;
        }

        if (!strcmp(argv[1], "-enc")){
                option = CAMELLIA_ENCRYPT;
        
        } else if (!strcmp(argv[1], "-dec")) {
                option = CAMELLIA_DECRYPT;
        }

        if(!strcmp(argv[2], "-ecb")) {
        	mode = ECB;
        } else if(!strcmp(argv[2], "-cbc")){
                mode = CBC;       
        }

        FILE *input_file = fopen(argv[3], "rb");
	char* input_buffer = malloc(CAMELLIA_BLOCK_SIZE);
	char* output_buffer = malloc(CAMELLIA_BLOCK_SIZE);

        FILE *output_file = fopen(argv[4], "wb"); 
	int bytes_read = 0;
	
	Camellia_set_key(argv[5], CAMELLIA_BLOCK_SIZE * 8, key);

	// encrypt
	while(!feof(input_file)) {
		bytes_read = fread(input_buffer, 1, CAMELLIA_BLOCK_SIZE, input_file);
		if(bytes_read < CAMELLIA_BLOCK_SIZE) {
			int i = 0;
			for(i = bytes_read; i<CAMELLIA_BLOCK_SIZE - 2; i++){
				input_buffer[i] = 0;		
			}
			input_buffer[CAMELLIA_BLOCK_SIZE] = CAMELLIA_BLOCK_SIZE - bytes_read;	
		}
		
		if(mode == ECB) {
			Camellia_ecb_encrypt(input_buffer, output_buffer, key, CAMELLIA_ENCRYPT);	
		} else if (mode == CBC) {
//			Camellia_cbc_encrypt(input_buffer, output_buffer, key, 	
		}

		fwrite(output_buffer, 1, CAMELLIA_BLOCK_SIZE, output_file);
	}
	
	return 0;
}
